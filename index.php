<?php
 require('inc/config.php');
 require('inc/DB.php');
 require('inc/functions.php');
 require('inc/navigation.php');
 require('inc/router.php');

 if($settings['main']['maintenance-mode'] == false){

    include('themes/' . $settings['main']['active-theme'] . '/main.tpl.php');

}else{
     echo "Svetainė išjungta";
}


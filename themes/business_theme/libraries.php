<?php
$activeTheme = $settings['main']['active-theme'];
$lib = [
    'css' => [
        'lib/bootstrap/dist/css/bootstrap.min',
        'lib/font-awesome/css/font-awesome.min',
        'lib/nprogress/nprogress',
        'lib/iCheck/skins/flat/green',
        'lib/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min',
        'lib/jqvmap/dist/jqvmap.min',
        'lib/bootstrap-daterangepicker/daterangepicker',
        'themes/'.$activeTheme.'/css/custom',
        'themes/'.$activeTheme.'/css/custom.min'
    ],
    'js' => [
        'lib/jquery/dist/jquery.min',
        'lib/bootstrap/dist/js/bootstrap.min',
        'lib/fastclick/lib/fastclick',
        'lib/nprogress/nprogress',
        'lib/Chart.js/dist/Chart.min',
        'lib/gauge.js/dist/gauge.min',
        'lib/bootstrap-progressbar/bootstrap-progressbar.min',
        'lib/iCheck/icheck.min',
        'lib/skycons/skycons',
        'lib/Flot/jquery.flot',
        'lib/Flot/jquery.flot.pie',
        'lib/Flot/jquery.flot.time',
        'lib/Flot/jquery.flot.stack',
        'libFlot/jquery.flot.resize',
        'lib/flot.orderbars/js/jquery.flot.orderBars',
        'lib/flot-spline/js/jquery.flot.spline.min',
        'lib/flot.curvedlines/curvedLines',
        'lib/DateJS/build/date',
        'lib/jqvmap/dist/jquery.vmap',
        'lib/jqvmap/dist/maps/jquery.vmap.world',
        'lib/jqvmap/examples/js/jquery.vmap.sampledata',
        'lib/jqvmap/examples/js/jquery.vmap.sampledata',
        'lib/moment/min/moment.min',
        'lib/bootstrap-daterangepicker/daterangepicker',
        'lib/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min',
        'lib/datatables.net/js/jquery.dataTables.min',
        'lib/datatables.net-bs/js/dataTables.bootstrap.min',
        'lib/datatables.net-buttons/js/dataTables.buttons.min',
        'lib/datatables.net-buttons-bs/js/buttons.bootstrap.min',
        'lib/datatables.net-buttons/js/buttons.flash.min',
        'lib/datatables.net-buttons/js/buttons.html5.min',
        'lib/datatables.net-buttons/js/buttons.print.min',
        'lib/dadatatables.net-fixedheader/js/dataTables.fixedHeader.min',
        'lib/datatables.net-keytable/js/dataTables.keyTable.min',
        'lib/datatables.net-responsive/js/dataTables.responsive.min',
        'lib/datatables.net-responsive-bs/js/responsive.bootstrap',
        'lib/datatables.net-scroller/js/dataTables.scroller.min',
        'lib/jszip/dist/jszip.min',
        'lib/pdfmake/build/pdfmake.min',
        'lib/pdfmake/build/vfs_fonts',
        'themes/'.$activeTheme.'/js/custom.min'
    ]
];

function add_library($type){

    global $lib;
    foreach($lib[$type] as $resource){
        if($type==='css'){
            echo  "<link href='$resource.css' rel='stylesheet'>\n";

        }elseif ($type==='js'){
            echo "<script src='$resource.js'></script>\n";
        }
    }
}


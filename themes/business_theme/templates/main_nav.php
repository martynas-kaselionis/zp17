<ul class="nav side-menu">
    <?php foreach($navigation as $navCat => $navItem): ?>
        <?php
        if($navCat == 'katalogas'){
            $className = "fa-home";
        } elseif ($navCat == 'katalogo-valdymas'){
            $className = "fa-edit";
        } elseif($navCat== 'ataskaitos'){
            $className = 'fa-desktop';
        } else{
            $className = "";
        }
        ?>
    <li><a><i class="fa <?php echo $className;?>"></i><?php echo ucfirst(str_replace('-',' ',$navCat)); ?><span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <?php foreach($navItem as $link => $linkTitle): ?>
            <li><a href="?page=<?php echo $link; ?>"><?php echo str_replace('-',' ',$linkTitle); ?> </a></li>
            <?php endforeach; ?>
        </ul>
    </li>
    <?php endforeach; ?>
</li>
</ul>
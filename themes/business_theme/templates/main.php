<div class="x_title">
    <h2><?php echo $pageName; ?></h2>
    <ul class="nav navbar-right panel_toolbox">
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
    </ul>
    <div class="clearfix"></div>
</div>
<div class="x_content">
   <?php echo $content; ?>
</div>

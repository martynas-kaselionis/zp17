<?php include('libraries.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $siteName; ?></title>

      <script src="lib/ckeditor/ckeditor.js"></script>

      <?php add_library('css'); ?>

  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-paw"></i> <span><?php echo $siteName; ?></span></a>
            </div>
            <div class="clearfix"></div>
            <!-- profilio info -->
            <?php include('templates/profile.php');?>
            <!-- /profilio info -->
            <br />
            <!-- asaido meniu-->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Produktų katalogas</h3>
                <?php include('templates/main_nav.php'); ?>
              </div>
            </div>
            <!-- /asaido meniu -->

            <!-- /mygtukai footrer-->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /mygtukai footer-->
          </div>
        </div>

        <!-- Navigacija -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt="">Čia bus tavo vardas
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;">Profilis</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Nustatymai</span>
                      </a>
                    </li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i>Atisijungti</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /Navigacija -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- Statistika -->
          <div class="row tile_count">
           <?php include('templates/statistics.php'); ?>
          </div>
          <!-- /statistika -->
          <br />
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <?php include('templates/main.php'); ?>
              </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
            </div>
          </div>
        </div>
        <!-- /puslapio-turinys -->

        <!-- footer content'as -->
        <footer>
          <div class="pull-right">
            <a href="https://colorlib.com">KITM</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content'as -->
      </div>
    </div>

    <?php add_library('js'); ?>
	
  </body>
</html>

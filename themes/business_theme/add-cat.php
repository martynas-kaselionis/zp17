<?php require_once('../inc/config.php'); ?>
<?php
$pavadinimas = $_POST['pavadinimas'];
$aprasymas = $_POST['aprasymas'];

if(isset($_POST['submit'])) {
    try {
        $stmt = "INSERT INTO kategorija (pavadinimas, aprasymas) VALUES (:pavadinimas, :aprasymas)";
        $querie = $pdo->prepare($stmt);
        $querie->execute(array($pavadinimas,$aprasymas));

        header('Location: index.php?page=nauja-kategorija');
        exit;
    } catch (Exception $e) {
        echo "Negaliu pridėti naujo įrašo";
        echo $e->getMessage();
        exit;
    }
}


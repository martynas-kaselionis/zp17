<?php
try {
    $stmt = $pdo->query('SELECT prekes.id, 
kategorija.pavadinimas AS kategorija,
 prekes.aprasymas, prekes.kaina, 
prekes.pavadinimas, prekes.kiekis FROM prekes
    INNER JOIN kategorija
    ON prekes.kategorijos_id = kategorija.id');
} catch (Exception $e) {
    echo $e;
    exit;
}
$data = $stmt->fetchAll();

?>

<table class="table table-bordered table-striped">
    <tr>
        <th>ID</th>
        <th>Kategorija</th>
        <th>Pavadinimas</th>
        <th>Aprašymas</th>
        <th>Nuotrauka</th>
        <th>Kiekis</th>
        <th>Kaina</th>
    </tr>
    <?php foreach ($data as $preke):?>
    <tr>
        <td><?php echo $preke['id'];?></td>
        <td><?php echo $preke['kategorija'];?></td>
        <td><?php echo $preke['pavadinimas'];?></td>
        <td><?php echo $preke['aprasymas'];?></td>
        <td><?php echo $preke['nuotrauka'];?></td>
        <td><?php echo $preke['kiekis'];?></td>
        <td><?php echo $preke['kaina'];?></td>
    </tr>
    <?php endforeach;?>
</table>


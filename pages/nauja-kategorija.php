<?php
$pavadinimas = $_POST['pavadinimas'];


if(isset($_POST['submit'])) {
    try {
        $stmt = "INSERT INTO kategorija (pavadinimas) 
VALUES (:pavadinimas)";
        $querie = $pdo->prepare($stmt);
        $querie->execute(array($pavadinimas));


    } catch (Exception $e) {
        echo "Negaliu pridėti naujo įrašo";
        $messages['error'] = $e->getMessage();
        exit;
    }
}

?>

<form method="POST" id="kategorija" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="true">

    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pavadinimas">Pavadinimas<span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="pavadinimas" required="required" class="form-control col-md-7 col-xs-12" name="pavadinimas">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aprasymas">Aprašymas<span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="aprasymas" name="aprasymas" required="required" class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button class="btn btn-primary" type="button">Atšaukti</button>
            <button class="btn btn-primary" type="reset">Valyti</button>
            <button type="submit" name="submit" class="btn btn-success">Išsaugoti</button>
        </div>
    </div>

</form>

<?php
try {
    $stmt = $pdo->query('SELECT * FROM kategorija');
} catch (Exception $e) {
    echo "Klaida: Negaliu gauti duomenų iš DB";
    exit;
}

$data = $stmt->fetchAll();
?>

<h2>Kategorijų sąrašas</h2>

<table id="datatable-keytable" class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>Pavadinimas</th>
        <th>Aprašymas</th>
        <th>Veiksmai</th>
    </tr>
    </thead>


    <tbody>
    <?php foreach($data as $item):?>
        <tr>
            <td><?php echo $item['pavadinimas']; ?></td>
            <td><?php echo $item['aprasymas']; ?></td>
            <td><a href="?<?php echo $_SERVER['QUERY_STRING']?>&delete=<?php echo $item['id'];?>" onclick="return confirm('Ar tikrai norite pašalinti?')">Šalinti</a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>




<form method="post">
    <label>Pavadinimas</label>
    <input type="text" name="pavadinimas">

    <input type="submit" name="submit"
           value="Saugoti">
</form>


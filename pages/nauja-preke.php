<?php
try {
    $stmt = $pdo->query('SELECT id, pavadinimas FROM kategorija');
} catch (Exception $e) {
    echo "Klaida: Negaliu gauti duomenų iš DB";
    exit;
}
$data = $stmt->fetchAll();

$name = $_POST['name'];
$description = $_POST['description'];
$quantity = $_POST['quantity'];
$price= $_POST['price'];
$category = $_POST['category'];


if(isset($_POST['submit'])) {
    if(!empty($_FILES['photo']))
    {
        $path = "uploads/";
        $path = $path . basename( $_FILES['photo']['name']);
        if(move_uploaded_file($_FILES['photo']['tmp_name'], $path)) {
            echo "Produkto nuotrauka ".  basename( $_FILES['photo']['name']).
                " įkelta sėkmingai";
            $photo = basename( $_FILES['photo']['name']);
        } else{
            echo "Klaida įkeliant nuotrauką";
        }
    }
    try {
        $stmt = "INSERT INTO prekes (pavadinimas, aprasymas,nuotrauka, kiekis, kaina, kategorijos_id) 
        VALUES (:name, :description, :photo,:quantity, :price, :category )";
        $querie = $pdo->prepare($stmt);
        $querie->execute(array($name,$description,$photo,$quantity,$price,$category));
    } catch (Exception $e) {
        echo "Negaliu pridėti naujo įrašo";
        echo $e->getMessage();
        exit;
    }
}
?>

<h2>Naujos prekės registracija</h2>

<form method="post" enctype="multipart/form-data" id="prekes-registracija" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Pavadinimas <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="name" name="name" required="required" class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category">Kategorija <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <select id="category" name="category" required="required" class="form-control col-md-7 col-xs-12">
                <option>Pasirinkite</option>
                <?php foreach($data as $val):?>
                    <option value="<?php echo $val['id'];?>"><?php echo $val['pavadinimas'];?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Aprašymas <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea name="editor1" id="editor1" rows="10" cols="80">
                Įveskite prekės aprašymą
            </textarea>
            <script>

                CKEDITOR.replace( 'editor1' );
            </script>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="photo">Nuotrauka <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="file" id="photo" name="photo" required="required" class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="quantity">Kiekis <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="quantity" name="quantity" required="required" class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Kaina <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="price" name="price" required="required" class="form-control col-md-7 col-xs-12">
        </div>
    </div>
    <div class="ln_solid"></div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button class="btn btn-primary" type="button">Atšaukti</button>
            <button class="btn btn-primary" type="reset">Valyti</button>
            <button type="submit" name="submit" class="btn btn-success">Išsaugoti</button>
        </div>
    </div>
</form>
<?php

//Navigation

$navigation= [
  //foreach
  'katalogas' => [
  //foreach
    'kompiuteriai' => 'Kompiuteriai',
    'telefonai' => 'Telefonai',
    'priedai' => 'Priedai'
   ],
  'katalogo-valdymas' => [
    'nauja-kategorija' => "Naujų kategorijų kūrimas",
    'nauja-preke' => 'Naujų prekių įvedimas',
    'visos-prekes' => 'Visos prekės',
    'prekes-sandelyje' => 'Prekės esančios sandėlyje',
    'duomenu-importas' => 'Duomenų importas',
    'duomenu-eksportas' => 'Duomenų eksportas',
  ],
  'ataskaitos' => [
    'ataskaitu-perziura' => 'Ataskaitų peržiūra',
    'ataskaitų-generavimas'=> 'Ataskaitų generavimas'
]
];
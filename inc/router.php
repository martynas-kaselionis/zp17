<?php
$page = htmlspecialchars($_GET['page']);
$file = 'pages/'.$page.'.php';
$pageName = "Prekių katalogas";
if($page) {
if (array_key_exists($page, $navigation['katalogas']) || array_key_exists($page, $navigation['katalogo-valdymas'])
|| array_key_exists($page, $navigation['ataskaitos']) && file_exists($file)) {
$pageName = ucfirst($page);
ob_start();
include($file);
$content = ob_get_contents();
ob_end_clean();
} else {
header('Location: 404.php');
exit;
}
}else{
$pageName = "Produktų katalogas";
$content = '<p>Mokomasis PHP projektas</p>';
}
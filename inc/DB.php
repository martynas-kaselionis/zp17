<?php

$host = "localhost";
$db = "produktai";
$charset= "utf8mb4";
$user = "root";
$pass = "";

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

try {
    $pdo = new PDO($dsn, $user, $pass, $opt);
} catch (Exception $e) {
    echo "Negaliu prisijungti prie DB<br>";
    echo $e->getMessage();
    exit;
}